package org.maponics.shapeintake.parcelapns;

import com.vividsolutions.jts.geom.Geometry;

public class ParcelApn {
	
	public final String theParcelApnValue;
	public int numPoints;
	public int numPolys;
	public Geometry polyGeometry;
	public Geometry pointGeometry;

	public ParcelApn(String newParcelApn ) {
		theParcelApnValue = newParcelApn;
		numPoints = 0;
		numPolys = 0;
		polyGeometry = null; 
		pointGeometry = null; 
	}

}
