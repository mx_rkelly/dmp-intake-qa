package org.maponics.shapeintake.parcelapns;

import java.util.HashMap;
import java.util.Map;

import com.vividsolutions.jts.geom.Geometry;

public class ParcelApns {
	
	private final Map<String,ParcelApn> apnMap;
	public int totalPoints;
	public int totalPolys;
	public int pointsWithoutApn;
	public int polysWithoutApn;
	
	public ParcelApns() {
		apnMap = new HashMap<String,ParcelApn>();
		totalPoints = 0;
		totalPolys = 0;
		pointsWithoutApn = 0;
		polysWithoutApn = 0;
	}
	
	private void storePointOrPoly(String parcelApnString, Geometry geom, boolean isPoint) {
		if ( ( parcelApnString == null ) || ( parcelApnString.isEmpty() ) ) {
			
			if ( isPoint ) {
				pointsWithoutApn++;
				totalPoints++;
			} else {
				polysWithoutApn++;
				totalPolys++;
			}
			
		} else {
			ParcelApn thisParcelApn = apnMap.get(parcelApnString);
			if ( thisParcelApn == null ) {
				thisParcelApn = new ParcelApn(parcelApnString);
				
				apnMap.put(parcelApnString, thisParcelApn);
			} 
		
			if ( isPoint ) {
				thisParcelApn.numPoints++;
				if ( thisParcelApn.numPoints == 1 ) {
					thisParcelApn.pointGeometry = geom;
				}
				totalPoints++;
			} else {
				thisParcelApn.numPolys++;
				if ( thisParcelApn.numPolys == 1 ) {
					thisParcelApn.polyGeometry = geom;
				}
				totalPolys++;
			}
			
//			if ( thisParcelApn.numPoints > 1 || thisParcelApn.numPolys > 1 ) {
//				System.out.println( "apn=" + parcelApnString + ", pts=" + thisParcelApn.numPoints + ", polys=" + thisParcelApn.numPolys );
//			}
		}

//		if ( isPoint ) {
//			System.out.println( "POINT, apn=" + parcelApnString + ", num=" + apnMap.size() + ", noApn=" + pointsWithoutApn );	
//		} else {
//			System.out.println( "POLY, apn=" + parcelApnString + ", num=" + apnMap.size() + ", noApn=" + polysWithoutApn );	
//		}
	}
	
	public void storePoint(String parcelApnString, Geometry geom) {
		storePointOrPoly(parcelApnString,geom,true);
	}
	
	public void storePoly(String parcelApnString, Geometry geom) {
		storePointOrPoly(parcelApnString,geom,false);
	}
	
	public java.util.List<ParcelApn> getApnsAsList() {
		return new java.util.ArrayList<ParcelApn>(apnMap.values());
	}

}
