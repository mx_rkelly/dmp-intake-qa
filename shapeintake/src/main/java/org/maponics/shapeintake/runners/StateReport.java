package org.maponics.shapeintake.runners;

import java.util.ArrayList;
import java.util.List;

import org.maponics.shapeintake.GlobalHandler;
import org.maponics.shapeintake.runners.Details.DETAIL_TYPE;

class StateReport {
	
	private final List<CountyReport> countyReportList;
	private Details details;
	private final String stateLabel;
	
	public StateReport(String stateLabel) {
		this.stateLabel = stateLabel;
		countyReportList = new ArrayList<CountyReport>();
		details = new Details(DETAIL_TYPE.STATE);
	}

	public void writeReport(GlobalHandler globalHandler) {
		details.writeReport(globalHandler, stateLabel);
		for(CountyReport report: countyReportList) {
			report.writeReport(globalHandler);
		}
	}
	
	public void addCountyReport(CountyReport countyReport) {
		countyReportList.add(countyReport);
		details = details.rollUpDetails(countyReport.getDetails());
	}

	public Details getDetails() {
		return details;
	}

	public int getSize() {
		return countyReportList.size();
	}

}
