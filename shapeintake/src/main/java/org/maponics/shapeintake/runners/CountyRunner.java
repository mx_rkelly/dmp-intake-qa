package org.maponics.shapeintake.runners;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;

import org.maponics.shapeintake.GlobalHandler;
import org.maponics.shapeintake.parcelapns.ParcelApn;
import org.maponics.shapeintake.parcelapns.ParcelApns;
import org.maponics.shapeintake.utilities.AreaCalculator;

import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.TopologyException;


class CountyRunner {

	private final CountyReport report;
	private final String dirpath;
	private final GlobalHandler globalHandler;
	private final String countyFips;
	private final boolean isZipped;

	private final ParcelApns myApnCollection;

	public CountyRunner(String dirpath, GlobalHandler globalHandler, String countyFips, boolean zipped) {
		this.dirpath = dirpath;
		this.globalHandler = globalHandler;
		this.countyFips = countyFips;
		this.isZipped = zipped;
		report = new CountyReport(this.countyFips);
		myApnCollection = new ParcelApns();
	}

	public void start() throws IOException {

		String pointsShapeFile;
		String polysShapeFile;
		String tempPath = null;

		if (isZipped) {
			File tempDir = new File(System.getProperty("java.io.tmpdir"));
			tempPath = tempDir + File.separator + "shapeIntakeGeowork";
			unZipIt(dirpath,tempPath);
			pointsShapeFile = tempPath + File.separator + "ParcelPoints_" + countyFips + ".shp";
			polysShapeFile = tempPath + File.separator + "Parcels_" + countyFips + ".shp";

		} else {
			pointsShapeFile = dirpath + File.separator + "ParcelPoints_" + countyFips + ".shp";
			polysShapeFile = dirpath + File.separator +"Parcels_" + countyFips + ".shp";
		}

		doProcess(pointsShapeFile, true);
		doProcess(polysShapeFile, false);

		Details details = report.getDetails();
		details.numPoints = myApnCollection.totalPoints;
		details.numPolys = myApnCollection.totalPolys;
		details.pointsWithoutApn = myApnCollection.pointsWithoutApn;
		details.polysWithoutApn = myApnCollection.polysWithoutApn;

		List<ParcelApn> apnList = myApnCollection.getApnsAsList();
		details.numUniqueApns = apnList.size();

		details.apnsWithBothPointAndPoly = 0;
		for (ParcelApn thisApn: apnList) {
			if (thisApn.numPoints > 0 && thisApn.numPolys > 0) {
				boolean badMatch = false;
				details.apnsWithBothPointAndPoly++;
				try {
					if ( thisApn.polyGeometry.covers(thisApn.pointGeometry)) {
						details.goodMatches++;
					} else {
						details.badMatches++;
						badMatch = true;
					}
				} catch (TopologyException ex) {
					details.badMatches++;
					badMatch = true;
				}
				
				if ( badMatch ) {
					globalHandler.reportGenericApnError(thisApn, countyFips, "is matched to external point" );
				}
			}
			if ( thisApn.numPolys > 1 ) {
				details.apnsWithMultiplePolys++;
				globalHandler.reportGenericApnError(thisApn, countyFips, "has " +  thisApn.numPolys + " polys");
			}
			if ( thisApn.numPoints > 1 ) {
				details.apnsWithMultiplePoints++;
				globalHandler.reportGenericApnError(thisApn, countyFips, "has " +  thisApn.numPoints + " points");
			}
		}
		
		System.out.println("County " + countyFips + ", base=" + dirpath + ", numPoints=" + details.numPoints + ", numPolys=" + details.numPolys );
		
		if ( isZipped ) {
			FileUtils.deleteDirectory(new File(tempPath));
		}
	}

	private void doProcess(String theFileName, boolean isPoints) throws IOException {

		File dataStoreFile = new File(theFileName);
		if (dataStoreFile.exists()) {
			FileDataStore store = FileDataStoreFinder.getDataStore(dataStoreFile);
			SimpleFeatureSource featureSource = store.getFeatureSource();
			SimpleFeatureCollection collection = featureSource.getFeatures();

			SimpleFeatureIterator iterator = collection.features();
			try {
				while (iterator.hasNext()) {
					SimpleFeature feature = iterator.next();

					String parcelApnAttr = (String) feature.getAttribute("PARCELAPN");

					Geometry thisGeom = (Geometry) feature.getDefaultGeometry();

					Details details = report.getDetails();
					if (isPoints) {					
				
						if ( !parcelApnAttr.isEmpty() ) {
							details.pointsWithParcelApn++;
						}

						{
							String addrScore = (String) feature.getAttribute("ADDRSCORE");

							if (addrScore.equals("1")) {
								details.addrScore1++;
							} else if (addrScore.equals("2")) {
								details.addrScore2++;
							} else if (addrScore.equals("3")) {
								details.addrScore3++;
							} else if (addrScore.equals("4")) {
								details.addrScore4++;
							} else if (addrScore.equals("5")) {
								details.addrScore5++;
							} else {
								details.addrScoreOther++;
							}
						}

						{
							String t = (String) feature.getAttribute("TAXAPN");
							if ( !t.isEmpty() ) details.pointsWithTaxApn++;
						}
						
						{
							String t1 = (String) feature.getAttribute("STHSNUM");
							String t2 = (String) feature.getAttribute("STDIR");
							String t3 = (String) feature.getAttribute("STSTNAME");
							String t4 = (String) feature.getAttribute("STSUFFIX");
							if ( !t1.isEmpty() || !t2.isEmpty() || !t3.isEmpty() || !t4.isEmpty() ) details.pointsWithStName++;
						}
						
					
						{
							String t = (String) feature.getAttribute("STQUADRANT");
							if ( !t.isEmpty() ) details.pointsWithStQuadrant++;
						}
						
						{
							String t = (String) feature.getAttribute("STUNITPRFX");
							if ( !t.isEmpty() ) details.pointsWithStUnitPrefix++;
						}
						
						{
							String t = (String) feature.getAttribute("STUNITNUM");
							if ( !t.isEmpty() ) details.pointsWithStUnitNum++;
						}
						
						{
							String t = (String) feature.getAttribute("STCITY");
							if ( !t.isEmpty() ) details.pointsWithStCity++;
						}
						
						{
							String t = (String) feature.getAttribute("STSTATE");
							if ( !t.isEmpty() ) details.pointsWithStState++;
						}
						
						{
							String t = (String) feature.getAttribute("STZIP");
							if ( !t.isEmpty() ) {
								details.pointsWithZip5++;
								String t4 = (String) feature.getAttribute("STZIP4");
								if ( !t4.isEmpty() ) details.pointsWithZip9++;
							}
							
						}

						myApnCollection.storePoint(parcelApnAttr, thisGeom );

					} else {

						myApnCollection.storePoly(parcelApnAttr, thisGeom );
						
						double squareMiles = AreaCalculator.getAreaInSquareMiles(thisGeom);
						double squareFeet = AreaCalculator.getSquareFeetFromSquareMiles(squareMiles);
						
						details.polySquareMiles+= squareMiles;
						
						if ( squareMiles > 100.0 ) {
							details.numPolysTooBig++;
							globalHandler.reportApnPolyError(thisGeom, countyFips, "Poly too big." );
						} else if ( squareFeet < 100.0 ) {
							details.numPolysTooSmall++;
							globalHandler.reportApnPolyError(thisGeom, countyFips, "Poly too small." );
						}
						
					}
				}
			} finally {
				iterator.close();
				store.dispose();
			}
		}
	}

	public CountyReport getReport() {
		return report;
	}

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *            input zip file
	 * @param outputFolder
	 *            zip file output folder
	 */
	private void unZipIt(String zipFile, String outputFolder) {

		byte[] buffer = new byte[1024];

		try {

			// create output directory if not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				boolean result = folder.mkdir();
				if (! result) {
					System.out.println("ERROR! Couldn't create output directory " + outputFolder + "! Exiting!");
					System.exit(1);
				}
			}

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				File parentDir = new File(newFile.getParent());
				if( ! parentDir.exists()) {
					boolean result = parentDir.mkdirs();
					if (!result) {
						System.out.println("ERROR! Couldn't create subdirectory " + parentDir + "! Exiting!");
						System.exit(1);
					}
				}
				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}


