package org.maponics.shapeintake.runners;

import java.io.File;
import java.io.IOException;

import org.maponics.shapeintake.GlobalHandler;

public class SingleDirectoryRunner {
	
	private final String dirpath;
	private final GlobalHandler globalHandler;
	private final AllStatesReport wholeReport;
	
	public SingleDirectoryRunner(String dirpath, GlobalHandler globalHandler) {
		this.dirpath = dirpath;
		this.globalHandler = globalHandler;
		wholeReport = new AllStatesReport();
	}

	public void start() throws IOException {

		File f = new File(dirpath);
		File[] filelist = f.listFiles();
		if (filelist == null) {
			System.out.println("ERROR! DMP product directory " + dirpath + " does not exist! Exiting!");
			System.exit(1);
		}

		System.out.println("SingleDirectoryRunner processing path=" + dirpath);
		System.out.println("SingleDirectoryRunner numFiles=" + filelist.length );

		for (String stateFips: getStateFips()) {
			System.out.println("SingleDirectoryRunner processing state " + stateFips);

			String filenamePrefixPattern = "Parcels_" + stateFips;

			StateReport stateReport = new StateReport("State_" + stateFips);

			for ( File thisFile: filelist) {
				if ( thisFile.getName().length() < 10 ) continue;

				if (thisFile.getName().substring(0, 10).equals(filenamePrefixPattern)) {

					String apparentFips = thisFile.getName().substring(8, 13);
					if (thisFile.isDirectory()) {
						// Then I work with it where it is, and no need to unzip then delete

						//System.out.println("idx=" + idx + ",  directory=" + thisFile.getName() );
						CountyRunner countyRunner = new CountyRunner(thisFile.getAbsolutePath(), globalHandler, apparentFips, false);
						countyRunner.start();
						stateReport.addCountyReport(countyRunner.getReport());

					} else if (thisFile.getName().substring(thisFile.getName().length() - 4).equalsIgnoreCase(".zip")) {
						// a zip file so I need to temporarily unpack it
						// System.out.println("idx=" + idx + ",  zip file=" + thisFile.getName() );
						CountyRunner countyRunner = new CountyRunner(thisFile.getAbsolutePath(), globalHandler, apparentFips, true);
						countyRunner.start();
						stateReport.addCountyReport(countyRunner.getReport());
					}
				}
			}

			if ( stateReport.getSize() > 0 ) {
				wholeReport.addStateReport(stateReport);
			}
		}
	}
	
	public AllStatesReport getReport() {
		return wholeReport;
	}

	private String[] getStateFips() {
		return new String[] {
			"01", "02", "04", "05", "06", "08", "09",
			"10", "11", "12", "13", "15", "16", "17", "18", "19",
			"20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
			"30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
			"40", "41", "42", "44", "45", "46", "47", "48", "49",
			"50", "51", "53", "54", "55", "56", "72"
		};
	}
}
