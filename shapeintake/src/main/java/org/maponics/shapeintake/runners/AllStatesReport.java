package org.maponics.shapeintake.runners;

import java.util.ArrayList;
import java.util.List;

import org.maponics.shapeintake.GlobalHandler;
import org.maponics.shapeintake.runners.Details.DETAIL_TYPE;

public class AllStatesReport {
	
	private final List<StateReport> stateReportList;
	private Details details;
	
	public AllStatesReport() {
		stateReportList = new ArrayList<StateReport>();
		details = new Details(DETAIL_TYPE.ALL);
	}
	
	public void writeReport(GlobalHandler globalHandler) {
		details.writeReport(globalHandler, "" );
		for (StateReport report: stateReportList) {
			report.writeReport(globalHandler);
		}
	}
	
	/* package */ void addStateReport(StateReport stateReport) {
		stateReportList.add(stateReport);
		details = details.rollUpDetails(stateReport.getDetails());
	}
	
}

