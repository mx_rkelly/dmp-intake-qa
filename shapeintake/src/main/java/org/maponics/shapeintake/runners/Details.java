package org.maponics.shapeintake.runners;

import org.maponics.shapeintake.GlobalHandler;

public class Details {
	
	public enum DETAIL_TYPE { ALL, STATE, COUNTY }

	private final DETAIL_TYPE detailType;
	public int countyCount;
	public int numUniqueApns;
	public int apnsWithBothPointAndPoly;
	public int numPoints;
	public int numPolys;
	public int pointsWithoutApn;
	public int polysWithoutApn;
	public int apnsWithMultiplePolys;
	public int apnsWithMultiplePoints;
	public int addrScore1;
	public int addrScore2;
	public int addrScore3;
	public int addrScore4;
	public int addrScore5;
	public int addrScoreOther;
	public double polySquareMiles;
	public int numPolysTooSmall;
	public int numPolysTooBig;
	public int pointsWithParcelApn;
	public int pointsWithTaxApn;
	public int pointsWithStName;
	public int pointsWithStQuadrant;
	public int pointsWithStUnitPrefix;
	public int pointsWithStUnitNum;
	public int pointsWithStCity;
	public int pointsWithStState;
	public int pointsWithZip5;
	public int pointsWithZip9;
	
	public int goodMatches;
	public int badMatches;
	
	public Details(DETAIL_TYPE detailType) {
		this.detailType = detailType;
		countyCount = 0;
		numUniqueApns = 0;
		apnsWithBothPointAndPoly = 0;
		numPoints = 0;
		numPolys = 0;
		pointsWithoutApn = 0;
		polysWithoutApn = 0;
		apnsWithMultiplePolys = 0;
		apnsWithMultiplePoints = 0;
		addrScore1 = 0;
		addrScore2 = 0;
		addrScore3 = 0;
		addrScore4 = 0;
		addrScore5 = 0;
		addrScoreOther = 0;
		polySquareMiles = 0;
		numPolysTooSmall = 0;
		numPolysTooBig = 0;
		
		pointsWithParcelApn = 0;
		pointsWithTaxApn = 0;
		pointsWithStName = 0;
		pointsWithStQuadrant = 0;
		pointsWithStUnitPrefix = 0;
		pointsWithStUnitNum = 0;
		pointsWithStCity = 0;
		pointsWithStState = 0;
		pointsWithZip5 = 0;
		pointsWithZip9 = 0;
		
		goodMatches = 0;
		badMatches = 0;

	}
	
	public Details rollUpDetails(Details b) {
		this.countyCount+= b.countyCount;
		this.numUniqueApns+= b.numUniqueApns;
		this.apnsWithBothPointAndPoly+= b.apnsWithBothPointAndPoly;
		this.numPoints+= b.numPoints;
		this.numPolys+= b.numPolys;
		this.pointsWithoutApn+= b.pointsWithoutApn;
		this.polysWithoutApn+= b.polysWithoutApn;
		this.apnsWithMultiplePolys+= b.apnsWithMultiplePolys;
		this.apnsWithMultiplePoints+= b.apnsWithMultiplePoints;
		this.addrScore1+= b.addrScore1;
		this.addrScore2+= b.addrScore2;
		this.addrScore3+= b.addrScore3;
		this.addrScore4+= b.addrScore4;
		this.addrScore5+= b.addrScore5;
		this.addrScoreOther+= b.addrScoreOther;
		this.polySquareMiles+= b.polySquareMiles;
		this.numPolysTooSmall+= b.numPolysTooSmall;
		this.numPolysTooBig+= b.numPolysTooBig;
		
		this.pointsWithParcelApn+= b.pointsWithParcelApn;
		this.pointsWithTaxApn+= b.pointsWithTaxApn;
		this.pointsWithStName+= b.pointsWithStName;
		this.pointsWithStQuadrant+= b.pointsWithStQuadrant;
		this.pointsWithStUnitPrefix+= b.pointsWithStUnitPrefix;
		this.pointsWithStUnitNum+= b.pointsWithStUnitNum;
		this.pointsWithStCity+= b.pointsWithStCity;
		this.pointsWithStState+= b.pointsWithStState;
		this.pointsWithZip5+= b.pointsWithZip5;
		this.pointsWithZip9+= b.pointsWithZip9;
		
		this.goodMatches+= b.goodMatches;
		this.badMatches+= b.badMatches;
		
		return this; 
	}
	
	public static String getReportHeader() {
		return "County FIPS\tState\tNum Counties\tNum Parcels\tParcels w/o APN\tNum Points\tPoints w/o APN\tUnique APNs\tAPNs Matches\tAPNs w/ Multiple Polys\t" +
				"APNs w/ Multiple Points\tAddrScore 1\tAddrScore 2\tAddrScore 3\tAddrScore 4\tAddrScore 5\tAddrScore Other\t" +
				"Poly Sq Miles\tNum Polys Too Big\tNum Polys Too Small\t" +
				"Num Points w/ Parcel APN\tNum Points w/ Tax APN\tNum Points w/ Street Name\tNum Points w/ St Quadrant\tNum Points w/ St Unit Prefix\t" +
				"Num Points w/ St Unit Number\tNum Points w/ City\tNum Points w/ State\tNum Points w/ Zip\tNum Points w/ Zip9\t" + 
				"Points Geometrically Matched\tPoints Geometrically mismatched";
	}
	
	public void writeReport(GlobalHandler globalHandler, String contextBasedLabel) {
		String labelColumn = "";
		String stateColumn = "";
		String countyCountColumn = "";
		
		switch (detailType) {
		case ALL: 
			labelColumn = "OVERALL TOTALS";
			stateColumn = contextBasedLabel;
			countyCountColumn = Integer.toString(countyCount);
			break;
		case STATE: 
			labelColumn = "STATE TOTALS";
			stateColumn = contextBasedLabel;
			countyCountColumn = Integer.toString(countyCount);
			break;
		case COUNTY: 
			labelColumn = contextBasedLabel;
			stateColumn = contextBasedLabel.substring(0, 2);
			countyCountColumn = "";
			break;
		default:
			break;
		}
		
		String s = labelColumn + "\t" + stateColumn + "\t" + countyCountColumn + "\t" + 
				numPolys + "\t" + polysWithoutApn + "\t" + 
				numPoints + "\t" + pointsWithoutApn + "\t" + 
				numUniqueApns + "\t" + apnsWithBothPointAndPoly + "\t" + apnsWithMultiplePolys + "\t" + apnsWithMultiplePoints + "\t" +
				addrScore1 + "\t" + addrScore2 + "\t" + addrScore3 + "\t" + addrScore4 + "\t" + addrScore5 + "\t" + addrScoreOther + "\t" +
				polySquareMiles + "\t" + numPolysTooBig + "\t" + numPolysTooSmall + "\t" +
				pointsWithParcelApn + "\t" + pointsWithTaxApn + "\t" + pointsWithStName + "\t" + pointsWithStQuadrant + "\t" + pointsWithStUnitPrefix + "\t" +
				pointsWithStUnitNum + "\t" + pointsWithStCity + "\t" + pointsWithStState + "\t" + pointsWithZip5 + "\t" + pointsWithZip9 + "\t" +
				goodMatches + "\t" + badMatches +
				"\t";
		
		globalHandler.writeToReportFile(s);

	}

}
