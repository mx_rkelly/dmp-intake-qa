package org.maponics.shapeintake.runners;

import org.maponics.shapeintake.GlobalHandler;
import org.maponics.shapeintake.runners.Details.DETAIL_TYPE;

public class CountyReport {
	
	private final String countyFips;
	private final Details details;
	
	public CountyReport(String countyFips) {
		this.countyFips = countyFips;
		details = new Details(DETAIL_TYPE.COUNTY);
		details.countyCount = 1;
	}
	
	public void writeReport(GlobalHandler globalHandler) {
		details.writeReport(globalHandler, countyFips);
	}

	public Details getDetails() {
		return details;
	}
}
