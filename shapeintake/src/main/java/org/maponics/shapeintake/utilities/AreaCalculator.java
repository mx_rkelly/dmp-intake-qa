package org.maponics.shapeintake.utilities;

import com.vividsolutions.jts.geom.Geometry;

public class AreaCalculator {
	
	public static double getAreaInSquareMiles(Geometry theGeom) {
		double conversion = (365222.0 * 365222.0);
		double squareFeet = theGeom.getArea() * conversion;
		return squareFeet / (5280.0 * 5280.0);
	}

	public static double getSquareFeetFromSquareMiles(double squareMiles) {
		return squareMiles * (5280.0 * 5280.0);
	}
	
	public static String getPrettyArea(Geometry theGeom) {
		String retString;
		double squareMiles = getAreaInSquareMiles(theGeom);
		
		if ( squareMiles > .001 ) {
			retString = String.format("%1$,.5f", squareMiles) + " sq mi";
		} else {
			retString = String.format("%1$,.5f", getSquareFeetFromSquareMiles(squareMiles)) + " sq ft";
		}
		
		return retString;
	}

}
