package org.maponics.shapeintake;

import java.io.File;
import java.io.IOException;

import org.maponics.shapeintake.runners.AllStatesReport;
import org.maponics.shapeintake.runners.SingleDirectoryRunner;

public class AppStartup {

	public static void main(String[] args) {
		if(args.length != 2 ) {
			String msg = "Usage: java -cp shapeintake.jar org.maponics.shapeintake.AppStartup <overall DMP path> <report path>";
			System.out.println(msg);
			System.exit(1);
		}

		String overallDirPath = args[0];
		String reportDir = args[1];

		System.out.println("Data path="+overallDirPath);
		System.out.println("Report path="+reportDir);

		try {
			File directory = new File(String.valueOf(overallDirPath));
			if (! directory.exists()) {
				System.out.println("ERROR! DMP product directory " + overallDirPath + " does not exist! Exiting!");
				System.exit(1);
			}

			directory = new File(String.valueOf(reportDir));
			if (! directory.exists()){
				boolean result = directory.mkdirs();
				if (! result) {
					System.out.println("ERROR! Couldn't create directory " + reportDir + "! Exiting!");
					System.exit(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		GlobalHandler globalHandler = new GlobalHandler(reportDir);

		SingleDirectoryRunner runner = new SingleDirectoryRunner(overallDirPath,globalHandler);
		try {
			runner.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AllStatesReport report = runner.getReport();
		report.writeReport(globalHandler);

		globalHandler.finish();
		
		System.out.println("Finished!");
		
		System.exit(0);
		
	}

}
