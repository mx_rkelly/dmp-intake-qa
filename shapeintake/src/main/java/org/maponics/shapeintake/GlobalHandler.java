package org.maponics.shapeintake;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.maponics.shapeintake.parcelapns.ParcelApn;
import org.maponics.shapeintake.runners.Details;
import org.maponics.shapeintake.utilities.AreaCalculator;

import com.vividsolutions.jts.geom.Geometry;

public class GlobalHandler {
	
	private BufferedWriter reportFileWriter;
	private BufferedWriter exceptionsFileWriter;
	
	public GlobalHandler(String outputPath) {
		try {
			reportFileWriter = new BufferedWriter(new FileWriter(outputPath + File.separator + "ParcelIntakeReport.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		writeToReportFile(Details.getReportHeader());

		try {
			exceptionsFileWriter = new BufferedWriter(new FileWriter(outputPath + File.separator + "ParcelIntakeExceptions.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public void finish() {
		
		try {
			reportFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			exceptionsFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeToReportFile(String s) {
		try {
			reportFileWriter.write(s + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeToExceptionsFile(String s) {
		try {
			exceptionsFileWriter.write(s + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void reportApnPolyError(Geometry geom, String countyFips, String msg) {
		writeToExceptionsFile( "County=" + countyFips +
				", polyRectangle=" + geom.getEnvelopeInternal() +
				", area=" + AreaCalculator.getPrettyArea(geom) +
				": " + msg );
	}
	
	public void reportGenericApnError(ParcelApn thisApn, String countyFips, String msg) {
		if ( thisApn.numPoints > 0 && thisApn.numPolys > 0 ) {
			writeToExceptionsFile( "County=" + countyFips + ", APN=" + thisApn.theParcelApnValue +
					", polyRectangle=" + thisApn.polyGeometry.getEnvelopeInternal() +
					", area=" + AreaCalculator.getPrettyArea(thisApn.polyGeometry) +
					", point=" + thisApn.pointGeometry.getCentroid() + 
					": " + msg );		
		} else if ( thisApn.numPolys > 0 ) {
			writeToExceptionsFile( "County=" + countyFips + ", APN=" + thisApn.theParcelApnValue +
					", polyRectangle=" + thisApn.polyGeometry.getEnvelopeInternal() +
					", area=" + AreaCalculator.getPrettyArea(thisApn.polyGeometry) +
					": " + msg );		
		} else if ( thisApn.numPoints > 0 ) {
			writeToExceptionsFile( "County=" + countyFips + ", APN=" + thisApn.theParcelApnValue +
					", point=" + thisApn.pointGeometry.getCentroid() + 
					": " + msg );
		}
		
		
	}
	
}
